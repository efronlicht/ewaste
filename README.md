# ewaste

## Table of Contents
- [ewaste](#ewaste)
  - [Table of Contents](#table-of-contents)
  - [Usage (non-interactive)](#usage-non-interactive)
  - [Usage (interactive)](#usage-interactive)
  - [installation:](#installation)

ewaste is a small, cross-platform, interactive command-line utility to waste system resources.
it is usable either interactively or by sending it commands via stdin.

## Usage (non-interactive)
```
ewaste [-cpu <cpu>] [-mem <mem>] [-disk <disk>]
```

Waste the specified amount of resources and then move into interactive mode.

## Usage (interactive)


Wait for commands on stdin. Commands are newline-separated. Commands are of the form:

- `cpu <cpu>`: waste a CPU thread until quit by finding prime numbers
- `mem <mem>`: waste memory until quit by allocating memory and writing random data at random indices to force the system to allocate memory
- `disk <disk>`: waste disk space until quit by writing to disk and syncing. the files are written to the temporary directory of the system, and are cleaned up when the program exits.


```Example usage```

## installation:
Binary releases are part of the source code, since i intend for this to be the only version, ever. Currently, the following binaries are available:

| OS | Architecture | Binary |
| --- | --- | --- |
| Windows | amd64 | [download](https://gitlab.com/efronlicht/ewaste/-/blob/master/bin/amd64_windows/ewaste) |
| Linux | amd64 | [download](https://gitlab.com/efronlicht/ewaste/-/blob/master/bin/amd64_linux/ewaste) |
| Linux | arm64 | [download](https://gitlab.com/efronlicht/ewaste/-/blob/master/bin/arm64_linux/ewaste) |
| macOS | amd64 | [download](https://gitlab.com/efronlicht/ewaste/-/blob/master/bin/amd64_darwin/ewaste) |
| macOS | arm64 | [download](https://gitlab.com/efronlicht/ewaste/-/blob/master/bin/arm64_darwin/ewaste) |
