package main

import (
	"bufio"
	"context"
	"flag"
	"fmt"
	"log"
	"math"
	"math/rand"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

// resource to waste: CPU, MEM, DISK
type Resource byte

const (
	Unknown Resource = iota
	CPU
	MEM
	DISK

	KB = 1000
	MB = KB * KB
	GB = MB * KB
	TB = GB * KB

	KiB int = 1024
	MiB     = KiB * KiB
	GiB     = MiB * KiB
	TiB     = GiB * KiB
)

var unit2int = map[string]int{
	"kb":  KB,
	"mb":  MB,
	"gb":  GB,
	"tb":  TB,
	"kib": KiB,
	"mib": MiB,
	"gib": GiB,
	"tib": TiB,
}

func parseInt(s string) (int, error) {
	s = strings.ToLower(s)
	for unit, mult := range unit2int {
		if s, ok := strings.CutSuffix(s, unit); ok {
			n, err := strconv.Atoi(strings.TrimSpace(s))
			if err != nil {
				return 0, err
			}
			return n * mult, nil
		}
	}
	return strconv.Atoi(s)
}

func fmtUnit(n int) string {
	switch {
	case n < KiB:
		return fmt.Sprintf("%3d B", n)
	case n < MiB:
		return fmt.Sprintf("%3d KiB", n/KiB)
	case n < GiB:
		return fmt.Sprintf("%3.2g2 MiB", float64(n)/float64(MiB))
	case n < TiB:
		return fmt.Sprintf("%3.2g GiB", float64(n)/float64(GiB))
	default:
		return fmt.Sprintf("%3.2g TiB", float64(n)/float64(TiB))
	}
}

func parseline(s string) (Resource, int, bool) {
	s = strings.ToLower(s)
	for _, v := range []struct {
		prefix string
		r      Resource
	}{
		{"cpu", CPU},
		{"mem", MEM},
		{"disk", DISK},
		{"c", CPU},
		{"m", MEM},
		{"d", DISK},
	} {
		if after, ok := strings.CutPrefix(s, v.prefix); ok {
			n, err := parseInt(strings.TrimSpace(after))
			if err != nil {
				continue
			}
			return v.r, n, true
		}
	}
	return Unknown, 0, false
}

// starting values for flags
var (
	cpu  = flag.Int("cpu", 0, "waste N cores")
	mem  = flag.String("mem", "0 KiB", "allocate N bytes of memory")
	disk = flag.String("disk", "0 KiB", "create a file of N bytes and sync it to disk")
)

func main() {
	flag.Parse()
	cpu := *cpu
	mem, err := parseInt(*mem)
	if err != nil {
		panic(fmt.Errorf("error parsing memory flag: %w", err))
	}
	disk, diskErr := parseInt(*disk)
	if diskErr != nil {
		panic(fmt.Errorf("error parsing disk flag: %w", err))
	}
	var wasted struct {
		cores int
		mem   struct {
			allocs int
			bytes  int
		}
		disk struct {
			files int
			bytes int
		}
	}
	printwasted := func() {
		fmt.Printf("cores: %d, memory: %d allocs, %s bytes, disk: %d allocs, %s bytes\n", wasted.cores, wasted.mem.allocs, fmtUnit(wasted.mem.bytes), wasted.disk.files, fmtUnit(wasted.disk.bytes))
	}
	ctx, cancel := context.WithCancel(context.Background())

	// if flags are set, start wasting resources immediately without waiting for user input
	if cpu != 0 {
		for i := 0; i < cpu; i++ {
			go findPrimes(ctx)
		}
		wasted.cores += cpu
	}
	if mem != 0 {
		buf := make([]byte, mem)
		buf[rand.Intn(mem)] = byte(rand.Intn(255))
		wasted.mem.allocs++
		wasted.mem.bytes += mem
	}
	if disk != 0 {
		f, err := os.CreateTemp("", "ewaste")
		if err != nil {
			panic(fmt.Errorf("error creating temp file: %w", err))
		}
		defer func() {
			f.Truncate(0)
			f.Close()
			os.Remove(f.Name())
		}()
		f.Write(make([]byte, disk))
		f.Sync()
	}
	if cpu != 0 || mem != 0 || disk != 0 {
		printwasted()
	}

	// start up interactive mode

	scanner := bufio.NewScanner(os.Stdin)
	scan := make(chan string)
	go func() {
		for scanner.Scan() {
			scan <- scanner.Text()
		}
	}()
	const usage = `usage: 
cpu N [unit]: waste N cores
mem N [unit]: allocate N bytes of memory
disk N [unit]: create a file of N bytes and sync it to disk
info: print current resource usage
quit: exit`
	log.Println(`waste some resources? ` + usage)
	defer cancel()

	var buf [][]byte
	var files []*os.File
	tmpdir, err := os.MkdirTemp("", "ewaste")
	if err != nil {
		panic(fmt.Errorf("error creating temp dir: %w", err))
	}

	for {
		var res Resource
		var n int
		select {
		case <-ctx.Done():
			log.Println("received signal, exiting")
			return
		case s := <-scan:
			s = strings.ToLower(strings.TrimSpace(s))
			switch s {
			case "":
				continue
			case "quit":
				cancel()
				continue
			case "info":
				continue
			default:
				var ok bool
				res, n, ok = parseline(s)
				if !ok {
					log.Printf("unknown resource %q", s)
					continue
				}
			}
		}
		switch res {
		case CPU:
			fmt.Printf("c %d\n", wasted.cores)
			for i := 0; i < n; i++ {
				go findPrimes(ctx)
			}
			wasted.cores += n
		case MEM:
			fmt.Printf("m %d %d\n", len(buf), n)
			buf = append(buf, make([]byte, n))
			buf[len(buf)-1][rand.Intn(n)] = byte(rand.Intn(255))
			wasted.mem.allocs++
			wasted.mem.bytes += n
		case DISK:
			name := filepath.Join(tmpdir, fmt.Sprintf("file%d", len(files)))
			f, err := os.Create(name)
			if err != nil {
				log.Println("error creating file:", err)
			}
			defer func() {
				f.Truncate(0)
				f.Close()
				os.Remove(name)
			}()

			fmt.Printf("f %d %d\n", len(files), n)
			files = append(files, f)
			files[len(files)-1].Write(make([]byte, n))
			files[len(files)-1].Sync()
			wasted.disk.files++
			wasted.disk.bytes += n
		}
		printwasted()

	}
}

func findPrimes(ctx context.Context) []int {
	primes := []int{2, 3, 5, 7, 11, 13, 17, 19, 23}
	for {
		select {
		case <-ctx.Done():
			return primes
		default:
			for i := 5; i < math.MaxInt32; i += 2 {
				for _, p := range primes {
					if i%p == 0 {
						primes = append(primes, i)
					}
				}
			}

		}
	}
}
