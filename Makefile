
all: amd64_linux amd64_osx amd64_windows arm64_osx arm64_linux


amd64_linux:
	mkdir -p bin/amd64_linux
	GOOS=linux GOARCH=amd64 go build -trimpath -o bin/amd64_linux/ewaste .
	strip bin/amd64_linux/ewaste
	upx bin/amd64_linux/ewaste

amd64_osx:
	mkdir -p bin/amd64_osx
	GOOS=darwin GOARCH=amd64 go build -trimpath -o bin/amd64_osx/ewaste .

amd64_windows:
	mkdir -p bin/amd64_windows
	GOOS=windows GOARCH=amd64 go build -trimpath -o bin/amd64_windows/ewaste.exe .
	strip bin/amd64_windows/ewaste.exe
	upx bin/amd64_windows/ewaste.exe


arm64_osx:
	mkdir -p bin/m1_osx
	GOOS=darwin GOARCH=arm64 go build -trimpath -o bin/m1_osx/ewaste .    
arm64_linux:
	mkdir -p bin/arm64_linux
	GOOS=linux GOARCH=arm64 go build -trimpath -o bin/arm64_linux/ewaste .
	upx bin/arm64_linux/ewaste
