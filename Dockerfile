FROM alpine:3.19
RUN apk add ca-certificates
COPY bin/amd64_linux/ewaste /ewaste
RUN chmod +x /ewaste
ENTRYPOINT ["/ewaste", "-cpu", "2", "-mem", "200 MiB", "-disk", "10 GiB"]